export default {
    ssr: false,
    target: "static",
    head: {
        titleTemplate: "%s - avalon",
        title: "avalon",
        meta: [
            { charset: "utf-8" },
            { name: "viewport", content: "width=device-width, initial-scale=1" },
            { hid: "description", name: "description", content: "" },
        ],
        link: [
            { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
            {
                href:
                    "https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap",
                rel: "stylesheet",
            },
            {
                href: "https://fonts.googleapis.com/css?family=Material+Icons",
                rel: "stylesheet",
            },
        ],
    },

    css: ["~/assets/style/app.scss"],
    plugins: ["~/plugins/firebase.js"],
    components: true,
    buildModules: ["@nuxtjs/vuetify"],
    modules: [],

    vuetify: {
        treeShake: true,
        defaultAssets: {
            font: {
                family: "Roboto",
            },
        },
        customVariables: ["~/assets/variables.scss"],
        theme: {
            themes: {
                light: {
                    primary: "#28BAE9",
                    secondary: "#A2DFFA",
                    accent: "#D7EEF9",
                    info: "#EFA655",
                    background: "#F4F7FA",
                    tablestripes: "#EFF5F8",
                },
            },
        },
    },
    build: {},
};
