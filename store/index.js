import firebase from "firebase/app";
import { auth, db } from "~/plugins/firebase.js";

//ROUNDSTATE
//0 = select players & confirm
//2 = vote for team
//3 = show votes
//4 = vote for mission
//5 = display status
//6 = victory/defeat
export const state = () => ({
    roundState: 0,
    user: null,
    users: [],
    owner: null,
    started: false,
    currentRound: 0,
    skipVotes: 0,
    currentKing: 0,
    gameType: 0,
    quests: [],
    selected: [],
});
export const getters = {
    gameEnded(state) {
        let acc = false;
        if (state.skipVotes >= 5) {
            acc = true;
        }
        const boolarr = state.quests.filter((x) => x.done);
        let truth = boolarr.reduce((a, v) => (v.success == true ? a + 1 : a), 0);
        let falses = boolarr.reduce((a, v) => (v.success == true ? a + 1 : a), 0);

        if (truth >= 3 || falses >= 3) {
            acc = true;
        }
        return acc;
    },
    getUser(state) {
        return state.user;
    },
    getRoundState(state) {
        return state.roundState;
    },
    getUsers(state) {
        return state.users;
    },
    getOwner(state) {
        return state.owner;
    },
    getState(state) {
        return state;
    },
    isItMyTurn(state, getters) {
        const idx = getters.getState.currentKing;
        return getters.getUsers[idx].id == getters.getUser.id;
    },
    myRoleKnows(state, getters) {
        const role = getters.getUsers.find((usr) => usr.id === getters.getUser.id).role;
        if (role == 1) {
            return getters.getUsers.filter((x) => x.role == 3 || x.role == 6 || x.role == 7 || x.role == 8);
        } else if (role == 2) {
            return getters.getUsers.filter((x) => x.role == 1 || x.role == 3);
        } else if (role == 3 || role == 6 || role == 7 || role == 4) {
            return getters.getUsers.filter((x) => (x.role == 3 || x.role == 6 || x.role == 7 || x.role == 4) && x.role != role);
        } else return null;
    },
    getRolesArray(state, getters) {
        let minions = 0;
        switch (getters.getState.gameType) {
            case 6:
                minions = 2;
                break;
            case 7:
                minions = 3;
                break;
            case 8:
                minions = 3;
                break;
            case 9:
                minions = 3;
                break;
            case 10:
                minions = 4;
                break;
            default:
                minions = 2;
                break;
        }
        let arr = [];
        //morgana
        arr.push(3);
        //mordred
        arr.push(4);
        //evil
        for (let i = 0; i < minions - 2; i++) {
            arr.push(6);
        }

        //merlin
        arr.push(1);
        //percival
        arr.push(2);
        //good
        for (let i = arr.length; i < getters.getState.gameType; i++) {
            arr.push(5);
        }
        return arr;
    },
};
export const mutations = {
    setUser(state, payload) {
        state.user = payload;
    },
    async setGameState(state, update) {
        Object.assign(state, update);
    },
    async incrementRoundState(state, gameKey) {
        await db
            .collection("games")
            .doc(gameKey)
            .update({
                roundState: firebase.firestore.FieldValue.increment(1),
            });
    },
};
export const actions = {
    async createGame({ commit, getters }) {
        const docid = Math.random().toString(36).substr(2, 9);
        defaultGame.owner = getters.getUser;
        defaultGame.users = [{ ...getters.getUser }];
        try {
            await db.collection("games").doc(docid).set(defaultGame);
            await commit("setGameState", defaultGame);
            return docid;
        } catch (e) {
            alert(e);
            return;
        }
    },

    async joinGame({ commit, getters }, gameKey) {
        return await db
            .collection("games")
            .doc(gameKey)
            .update({
                users: firebase.firestore.FieldValue.arrayUnion(getters.getUser),
            });
    },

    async getGame({ commit }, gameKey) {
        return await db.collection("games").doc(gameKey).get();
    },

    async startGame({ commit, getters }, gameKey) {
        await db
            .collection("games")
            .doc(gameKey)
            .update({ started: true, gameType: getters.getUsers.length });
        const userArr = getters.getUsers.slice(0);
        let rolesArr = getters.getRolesArray.slice(0);
        for (let i = rolesArr.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [rolesArr[i], rolesArr[j]] = [rolesArr[j], rolesArr[i]];
        }
        userArr.forEach((usr, i) => {
            usr.role = rolesArr[i];
            usr.hasVoted = false;
            usr.vote = false;
        });
        await db.collection("games").doc(gameKey).update({ users: userArr });
    },
    async selectPlayer({ commit, getters }, selection) {
        if (getters.getState.selected.includes(selection.playerId)) {
            await db
                .collection("games")
                .doc(selection.gameKey)
                .update({
                    selected: firebase.firestore.FieldValue.arrayRemove(selection.playerId),
                });
        } else {
            await db
                .collection("games")
                .doc(selection.gameKey)
                .update({
                    selected: firebase.firestore.FieldValue.arrayUnion(selection.playerId),
                });
        }
    },
    async confirmSelect({ commit }, gameKey) {
        commit("incrementRoundState", gameKey);
    },
    async roundFinish({ commit }, round) {
        var ref = db.collection("games").doc(round.gameKey);

        db.runTransaction((transaction) => {
            return transaction.get(ref).then((t) => {
                if (!t.exists) {
                    console.log("TransactionError");
                }
                let userArr = t.data().users;
                userArr.forEach((usr) => {
                    usr.hasVoted = false;
                    usr.vote = false;
                });
                let quests = t.data().quests;
                quests[t.data().currentRound].done = true;
                quests[t.data().currentRound].success = round.round;

                transaction.update(ref, {
                    selected: [],
                    quests: quests,
                    users: userArr,
                    roundState: 0,
                    currentKing: getters.getState.currentKing == getters.getUsers.length - 1 ? 0 : firebase.firestore.FieldValue.increment(1),
                });
            });
        });
    },

    async skipVote({ commit, getters }, gameKey) {
        var ref = db.collection("games").doc(gameKey);

        db.runTransaction((transaction) => {
            return transaction.get(ref).then((t) => {
                if (!t.exists) {
                    console.log("TransactionError");
                }
                let userArr = t.data().users;
                userArr.forEach((usr) => {
                    usr.hasVoted = false;
                    usr.vote = false;
                });

                transaction.update(ref, {
                    users: userArr,
                    skipVotes: firebase.firestore.FieldValue.increment(1),
                    roundState: 0,
                    currentKing: getters.getState.currentKing == getters.getUsers.length - 1 ? 0 : firebase.firestore.FieldValue.increment(1),
                });
            });
        });
    },
    async vote({ commit, getters }, vote) {
        var ref = db.collection("games").doc(vote.gameKey);

        db.runTransaction((transaction) => {
            return transaction.get(ref).then((t) => {
                if (!t.exists) {
                    console.log("TransactionError");
                }
                let userArr = t.data().users;
                userArr.find((usr) => usr.id === getters.getUser.id).hasVoted = true;
                userArr.find((usr) => usr.id === getters.getUser.id).vote = vote.vote;

                transaction.update(ref, { users: userArr });
            });
        });
    },
    revealVotes({ commit }, gameKey) {
        commit("incrementRoundState", gameKey);
    },
    countVotes({ commit }, gameKey) {
        commit("incrementRoundState", gameKey);
    },

    async initiateVoteRound({ commit }, gameKey) {
        var ref = db.collection("games").doc(gameKey);

        db.runTransaction((transaction) => {
            return transaction.get(ref).then((t) => {
                if (!t.exists) {
                    console.log("TransactionError");
                }
                let userArr = t.data().users;
                userArr.forEach((usr) => {
                    usr.hasVoted = false;
                    usr.vote = false;
                });

                transaction.update(ref, { users: userArr, roundState: firebase.firestore.FieldValue.increment(1) });
            });
        });
    },

    async signInWithFacebook() {
        await auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
    },

    signOut() {
        auth.signOut();
    },

    initializeAuth({ commit }) {
        auth.onAuthStateChanged((user) => {
            if (user) {
                commit("setUser", { name: user.displayName, id: user.uid });
            } else {
                commit("setUser", null);
            }
        });
    },

    async initializeGameLive({ commit }, gameKey) {
        try {
            await db
                .collection("games")
                .doc(gameKey)
                .onSnapshot((resp) => {
                    commit("setGameState", resp.data());
                });
        } catch (error) {
            console.log("WHAT HAPPENED");
        }
    },
};

const defaultGame = {
    roundState: 0,
    owner: null,
    users: null,
    started: false,
    currentRound: 0,
    skipVotes: 0,
    currentKing: 0,
    gameType: 0,
    quests: [
        { id: 1, success: false, done: false },
        { id: 2, success: false, done: false },
        { id: 3, success: false, done: false },
        { id: 4, success: false, done: false },
        { id: 5, success: false, done: false },
    ],
    selected: [],
};
