import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyA17c6osfOh6lulxDbfSTQ6whMOuuSasPY",
    authDomain: "avalon-osmai.firebaseapp.com",
    projectId: "avalon-osmai",
    storageBucket: "avalon-osmai.appspot.com",
    messagingSenderId: "915273411651",
    appId: "1:915273411651:web:95eedab79a0921b718f8f6",
};
// Initialize Firebase
!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : "";
export const auth = firebase.auth();
export const db = firebase.firestore();
export default firebase;
