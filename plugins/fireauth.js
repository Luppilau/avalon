import { auth } from "@/services/fireinit.js"
import { Promise } from "core-js"
import { resolve } from "core-js/fn/promise"

export default context => {
    const {store} = context

    return new Promise((resolve, reject) => {
        auth.onAuthStateChanged(user => {
            if(user){
                return resolve(store.commit('setUser', user))
            }
            return resolve()
        })
    }
}