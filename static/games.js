//1 = merlin
//2 = percival
//3 = morgana
//4 = mordred
//5 = good
//6 = evil
//7 = assassin
//8 = oberon

const fivePlayers = {
    quests: [
        { players: 2, required: 1 },
        { players: 3, required: 1 },
        { players: 2, required: 1 },
        { players: 3, required: 2 },
        { players: 3, required: 1 },
    ],
    minions: 2,
};
const sixPlayers = {
    quests: [
        { players: 2, required: 1 },
        { players: 3, required: 1 },
        { players: 4, required: 1 },
        { players: 3, required: 1 },
        { players: 4, required: 1 },
    ],
    minions: 2,
};
const sevenPlayers = {
    quests: [
        { players: 2, required: 1 },
        { players: 3, required: 1 },
        { players: 3, required: 1 },
        { players: 4, required: 2 },
        { players: 4, required: 1 },
    ],
    minions: 3,
};
const eightPlayers = {
    quests: [
        { players: 3, required: 1 },
        { players: 4, required: 1 },
        { players: 4, required: 1 },
        { players: 5, required: 2 },
        { players: 5, required: 1 },
    ],
    minions: 3,
};
const ninePlayers = {
    quests: [
        { players: 3, required: 1 },
        { players: 4, required: 1 },
        { players: 4, required: 1 },
        { players: 5, required: 2 },
        { players: 5, required: 1 },
    ],
    minions: 3,
};
const tenPlayers = {
    quests: [
        { players: 3, required: 1 },
        { players: 4, required: 1 },
        { players: 4, required: 1 },
        { players: 5, required: 2 },
        { players: 5, required: 1 },
    ],
    minions: 4,
};

const games = { fivePlayers, sixPlayers, sevenPlayers, eightPlayers, ninePlayers, tenPlayers };

export default games;
